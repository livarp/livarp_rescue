#! /bin/bash
# livarp-rescue ratpoison start-up script
#########################################

## setup auto-mounting -------------------------------------------------
sleep 10 && udisks-glue --session &

## launch panel --------------------------------------------------------
fbpanel &

## setup network -------------------------------------------------------
nm-applet &

## launch ratpoison ----------------------------------------------------
exec ratpoison
