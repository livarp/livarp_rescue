################################
## fbpanel configuration file ##
##      livarp_0.4 menu       ##
################################
# DESCRIPTION
# Configuration file consists of mandatory 'Global' block that MUST come first,
# and optionally one or more 'Plugin' block.
# Lines having '#' as first non-blank char or blank lines are ignored
# Keywords are not case-sensitive
# Values are case-sensitive
# Value of variable is a text from first non-blank char after '='
# till the last non-blank char. '#' is NOT treated as coment in this context

# 'Global' block describes global parameters like position, size and
# some NETWM settings

# Global {

#     # screen edge
#     # legal values are: left, right, top, bottom
#     edge = bottom

#     # allignment of a panel
#     # legal values are: left, right, center
#     allign = left

#     # length of margin (in pixels)
#     # legal values are numbers
#     margin = 0

#     # widthtype specifies how panel width is calculated
#     # legal values are: request, pixel, percent
#     #   request - follow widgets' size requests. can shrink or grow dynamically
#     #   pixel   - occupy fixed number of pixels, then 'width' variable holds a number
#     #   percent - be 'width' precent of an edge.
#     widthType = percent

#     # numerical value of width (not applicable for 'request' widthtype)
#     # legal values are numbers
#     width = 80

#     # heighttype specifies how panel height is calculated
#     # legal values are: pixel
#     #   pixel   - ocupy fixed number of pixels, then 'height' variable holds a number
#     heightType = pixel

#     # numerical value of height (if applicable)
#     # legal values are numbers
#     height = 28


#     # Identify panel window type as dock
#     # legal values are boolean
#     setDockType = true

#     # Reserve panel's space so that it will not be covered by maximazied windows
#     # legal values are boolean
#     # setPartialStrut = true


#     # Transparency stuff:
#     # tintColor is a color to composite on root background given as #RRGGBB or as name
#     # alpha is transparency of the tint color.
#     # transparent = true
#     # tintColor = #FFFFFF
#         or
#     # tintColor = white
#     # alpha = 127

#     # Autohide
#     # autoHide = false
#     # heightWhenHidden = 2

# }



# 'Plugin' block specifies a plugin to load. It has same syntax for both
# builtin and external plugins.

# First parameter is 'type'. It's mandatory and must come first
# Legal values are plugin names. Names of builtin plugins are:
# separator   - visual separator
# wincmd      - 'show desktop' button
# taskbar     - lists all opened windows (tasks)
# launchbar   - bar with launch button
# image       - just shows an image
# dclock      - digital clock
# space       - just seize space
# pager       - thumbnailed view of the desktop
# tray        - tray for XEMBED icons (aka docklets)

# expand - specifies if plugin can accomodate extra space or not [optional]
# padding - extra padding around plugin  [optional]
# config {} - block of plugin's private configuration.
#             This part is plugin  dependant


#
# Plugin {
#     type = wincmd
#     config {
#         image = ~/.fbpanel/images/Desktop2.png
#         tooltip = Left click to iconify all windows. Middle click to shade them.
#     }
# }

## Config Start ##

Global {
    edge = top
    allign = left
    margin = 0
    widthtype = request
    width = 100
    height = 18
    transparent = true
    tintcolor = #222222
    alpha = 255
    setdocktype = true
    setpartialstrut = true
    autohide = true
    heightWhenHidden = 0
    roundcorners = false
    roundcornersradius = 1
    layer = none
    MaxElemHeight = 32
}

Plugin {
    type = menu
    config {
        IconSize = 18
        #icon = start-here
        image = ~/.config/fbpanel/icons/livarp.png
        item {
            name = terminal
            icon = terminal
            action = urxvtcd
        }
        item {
            name = root terminal
            icon = gksu-root-terminal
            action = gksu urxvtcd
        }
        item {
            name = file-manager
            icon = file-manager
            action = rox-filer
        }
        item {
            name = web browser
            icon = links2
            action = xlinks2 http://arpinux.org/livarp_start
        }
        item {
            name = tool-list
            icon = applications-utilities
            action = urxvtcd -e most .tool_list
        }
        separator {
        }
        menu {
            name = cli-apps
            icon = terminal
            item {
                name = terminal
                icon = terminal
                action = urxvtcd
            }
            item {
                name = root terminal
                icon = gksu-root-terminal
                action = gksu urxvtcd
            }
            item {
                name = file-manager
                icon = file-manager
                action = urxvtcd -e ranger
            }
            item {
                name = editor
                icon = vim
                action = urxvtcd -e vim
            }
            item {
                name = music
                icon = gnome-sound-properties
                action = urxvtcd -e mocp
            }
            item {
                name = web browser
                icon = links2
                action = urxvtcd -e links2 http://arpinux.org/livarp_start
            }
            item {
                name = ncftp
                icon = browser
                action = urxvtcd -e ncftp
            }
            item {
                name = disk usage
                icon = baobab
                action = urxvtcd -e ncdu
            }
        }
        menu {
            name = applications
            icon = applications-accessories
            menu {
                name = internet
                icon = applications-internet
                item {
                    name = xlinks2
                    icon = links2
                    action = xlinks2 http://arpinux.org/livarp_start
                }
                item {
                    name= claws-mail
                    icon = claws-mail
                    action = claws-mail
                }
                item {
                    name = weechat
                    icon = pidgin
                    action = urxvtcd -e weechat-curses
                }
                item {
                    name = filezilla
                    icon = filezilla
                    action = filezilla
                }
                item {
                    name = transmission
                    icon = transmission
                    action = transmission-gtk
                }
            }
            menu {
                name = graphics
                icon = gnome-graphics
                item {
                    name = gpicview
                    icon = gthumb
                    action = gpicview
                }
                item {
                    name = select color
                    icon = color-line
                    action = xcolorsel
                }
                item {
                    name = select font
                    icon = fonts
                    action = xfontsel
                }
                menu {
                    name = screenshot
                    icon = gnome-screenshot
                    item {
                        name = now
                        icon = desktop
                        action = screenshot -a
                    }
                    item {
                        name = in 5 sec.
                        icon = emblem-urgent
                        action = screenshot -d
                    }
                    item {
                        name = active window in 2 sec..
                        icon = display-capplet
                        action = screenshot -w
                    }
                    item {
                        name = select a zone
                        icon = stock_leave-fullscreen
                        action = screenshot -z
                    }
                }
            }
            menu {
                name = media
                icon = applications-multimedia
                item {
                    name = mplayer
                    icon = mplayer
                    action = gnome-mplayer
                }
                item {
                    name = mocp
                    icon = gnome-sound-properties
                    action = urxvtcd -e mocp
                }
                item {
                    name = moc config
                    icon = gtk-edit
                    action = geany .moc/config
                }
                item {
                    name = xfburn
                    icon = brasero
                    action = xfburn
                }
                item {
                    name = volume
                    icon = gnome-settings-sound
                    action = urxvtcd -T sound -e alsamixer
                }
            }
            menu {
                name = office
                icon = applications-office
                item {
                    name = text editor
                    icon = geany
                    action = geany
                }
                item {
                    name = pdf viewer
                    icon = evince
                    action = epdfview
                }
                item {
                    name = agenda
                    icon = calendar
                    action = urxvtcd -e calcurse
                }
                item {
                    name = vim
                    icon = vim
                    action = urxvtcd -T editor -e vim
                }
            }
            menu {
                name = utils
                icon = applications-utilities
                item {
                    name = catfish
                    icon = gnome-searchtool
                    action = catfish
                }
                item {
                    name = archiver
                    icon = file-roller
                    action = file-roller
                }
                item {
                    name = calculator
                    icon = gnome-calculator
                    action = xcalc
                }
                item {
                    name = htop
                    icon = htop
                    action = urxvtcd -e htop
                }
                item {
                    name = xosview
                    icon = xosview
                    action = xosview
                }
                item {
                    name = pyrenamer
                    icon = pyrenamer
                    action = pyrenamer
                }
                item {
                    name = fslint
                    icon = fslint_icon
                    action = fslint-gui
                }
                item {
                    name = force quit
                    icon = force-exit
                    action = xkill
                }
            }
        }
        menu {
            name = configuration
            icon = preferences-other
            item {
                name = user interface
                icon = gnome-settings-theme
                action = lxappearance
            }
            item {
                name = volume
                icon = gnome-settings-sound
                action = urxvtcd -T sound -e alsamixer
            }
            item {
                name = display
                icon = gnome-display-properties
                action = arandr
            }
            menu {
                name = screensaver
                icon = xscreensaver
                item {
                    name = config screensaver
                    icon = drakconf
                    action = xscreensaver-demo
                }
                item {
                    name = reload screensaver
                    icon = gtk-refresh
                    action = xscreensaver-command -restart
                }
                item {
                    name = disable screensaver
                    icon = dialog-close
                    action = xscreensaver-command -exit
                }
            }
            menu {
                name = fbpanel
                icon = gnome-settings-ui-behavior
                item {
                    name = edit config
                    icon = drakmenustyle
                    action = geany .config/fbpanel/default
                }
                item {
                    name = reload fbpanel
                    icon = gtk-refresh
                    action = killall -USR1 fbpanel
                }
            }
        }
        menu {
            name = ratpoison
            image = ~/.config/fbpanel/icons/ratpoison.png
            item {
                name = edit startup script
                icon = gtk-edit
                action = geany .ratpoison_start.sh
            }
            item {
                name = edit ratpoisonrc
                icon = gtk-edit
                action = geany .ratpoisonrc
            }
            item {
                name = restart rapoison
                icon = gtk-refresh
                action = ratpoison -c restart
            }
            item {
                name = man ratpoison
                icon = terminal
                action = urxvtcd -e man ratpoison
            }
            item {
                name = wiki ratpoison (fr/us)
                icon = system-help
                action = xlinks2 /usr/share/livarp/help_center/ratpoison.html
            }
            item {
                name = ratpoison main page
                icon = web-browser
                action = xlinks2 http://www.nongnu.org/ratpoison/
            }
            item {
                name = ratpoison keys
                icon = gnome-settings-accessibility-keyboard
                action = .ratpoison/ratkeys.sh
            }
        }
        menu {
            name = system
            icon = applications-system
            item {
                name = gparted
                icon = gparted
                action = gksu gparted
            }
            item {
                name = disk usage
                icon = baobab
                action = urxvtcd -e ncdu
            }
            item {
                name = package manager
                icon = synaptic
                action = gksu synaptic
            }
            item {
                name = root terminal
                icon = gksu-root-terminal
                action = gksu urxvtcd
            }
            item {
                name = info system
                icon = hardinfo
                action = hardinfo
            }
        }
        separator {
        }
        item {
            name = exit
            icon = gnome-shutdown
            action = dmenu-quit.sh
        }
    }
}
Plugin {
    type = space
    config {
        size = 5
    }
}

Plugin {
    type = tray
}
Plugin {
    type = space
    config {
        size = 5
    }
}

# 'icons' plugin lets you customize window icons.
# these changes apply to entire desktop
Plugin {
    type = icons
    config {
        DefaultIcon = /usr/share/fbpanel/images/default.xpm
        application {
            icon = gnome-terminal
            ClassName = XTerm
        }
        application {
            icon = gnome-terminal
            ClassName = mlterm
        }
        application {
            icon = gnome-terminal
            ClassName = URxvt
        }
        application {
            icon = gnome-emacs
            ClassName = Emacs
        }
        application {
            icon = mozilla-firefox
            ClassName = Firefox-bin
        }
        application {
            icon = mozilla-firefox
            ClassName = Firefox
        }
    }
}
## Config End ##
